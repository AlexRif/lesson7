package ru.example.hibernateexample.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.example.hibernateexample.Entity.User;
import ru.example.hibernateexample.Service.UserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String getHome(){
        return "html/homePage";
    }

    @GetMapping("/getUsers")
    public String getUsers(Model model){
        List<User> users = userService.getAllUsers();
        model.addAttribute("users",users);
        return "ftlh/usersList";
    }

    @PostMapping( value = "/createUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
    produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public String createUser(User user){
        userService.createUser(user);
        return "redirect:/getUsers";
    }
}
