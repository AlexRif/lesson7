package ru.example.hibernateexample.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotNull
    @Size(min = 5, max = 30, message = "Имя пользователя не может быть меньше 5 и больше 30 симолов")
    private String username;

    @NotNull
    @Size(min = 8, max = 30, message = "Пароль не может быть меньше 8 и больше 30 симолов")
    private String password;

    @Size(min = 3, max = 15)
    private String firstName;
}
